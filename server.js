var express = require('express');
const bodyParser = require("body-parser");
var app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

var fs = require("fs");
const mysql = require('mysql');
var querystring = require('querystring');
// First you need to create a connection to the DB
const con = mysql.createConnection({host: 'localhost',user: 'root',password: 'admin',database: 'cicd'}); 

app.get('/otp', function (req, res) {
   var userId = req.param('userId');
   console.log('userId = '+userId);
   var otp = getRandomInt(9)+""+getRandomInt(9)+""+getRandomInt(9)+""+getRandomInt(9);
   console.log(otp);
   
   var sqlSelectQuery = "select otp from users_otp where user_id ='"+userId+"' and otp_verified = 'N';"
   con.query(sqlSelectQuery, function(error1, results1, fields1){
		if(error1){
			res.send(error1);
		}
		console.log(results1);
		var tempOTP = results1[0];
		console.log('tempOTP = '+tempOTP);
		if(tempOTP != null && tempOTP != undefined && tempOTP.otp != undefined){
			res.json({'otp':tempOTP.otp});			
		}else{
			var sqlInsertQuery = "insert into users_otp(user_id, otp, otp_verified) values ('"+userId+"', '"+otp+"','N');"
			con.query(sqlInsertQuery, function (error2, results2, fields2){
				if(error2){
					res.send(error2);
				}
				res.json({'otp':otp});			
			});  
		}
	});
   
   
})

app.post('/otp', function (req, res) {
   console.log('inside post method..');
   var userId = req.body.userId;
   var otp = req.body.otp;
   console.log('userId = '+userId+"   OTP = "+otp);
   
   var sqlSelectQuery = "select otp from users_otp where user_id ='"+userId+"' and otp_verified = 'N';"
   var sqlUpdateQuery = "update users_otp set otp_verified = 'Y' where user_id ='"+userId+"' and otp_verified = 'N' and otp = '"+otp+"';"
   
   con.query(sqlSelectQuery, function (error1, results1, fields1){
		if(error1){
			res.send(error1);
		}
		var tempOTP = results1[0];
		
		if(tempOTP!= undefined && tempOTP.otp!=undefined && otp === tempOTP.otp){
			con.query(sqlUpdateQuery, function(error2, results2, fields2){
				if(error2){
					res.send(error2);
				}
				res.json({'verified':true});
			});
		}else{
			res.json({'verified':false});
		}
	});  
})

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

var server = app.listen(8082, function () {

  var host = server.address().address
  var port = server.address().port

  console.log("Web Services Application listening at http://%s:%s", host, port)

})